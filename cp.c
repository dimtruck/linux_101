#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include "lib/tlpi_hdr.h"

void copy(int input_fd, int output_fd) {
    // each block will be 64 bytes
    size_t len = 64;
    char *buf;
    ssize_t numRead, numWritten;

    while(TRUE) {
        // next, read from stdin
        // allocate 64 bytes to buffer
        buf = malloc(len);
        if (buf == NULL)
            errExit("malloc");
        // read from input fd up to 64 bytes
        numRead = read(input_fd, buf, len);
        // check if failed to read or end of file
        if (numRead == -1)
            errExit("read");
        if (numRead == 0) {
            printf("end-of-file\n");
            break;
        } else {
            // write up to 64 bytes to file
            numWritten = write(output_fd, buf, numRead);
            if (numWritten == -1)
                errExit("write");
        }

        free(buf);
    }
}

/*
tee command
parameters are:
<input_file> - file to copy from
<output_file> - file to copy to
*/
int main(int argc, char *argv[])
{
    int input_fd, output_fd;
    // make sure enough arguments are provided
    if (argc > 1 ) {
        // check if second arg is help and output manual
        if (strcmp(argv[1], "--help") == 0) {
            usageErr("%s input_file output_file\n",
                    argv[0]);
        } else if (argc != 3) {
                cmdLineErr("Must have input and output files only\n");
        } else {
            // open file with read only
            input_fd = open(argv[1], O_RDONLY,
                S_IRUSR | S_IRGRP | S_IROTH);                     /* rw-rw-rw- */
            if (input_fd == -1)
                errExit("input filename open");
            // open file with read write + create + truncate
            output_fd = open(argv[2], O_RDWR | O_CREAT | O_TRUNC,
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                        S_IROTH | S_IWOTH);                     /* rw-rw-rw- */
            if (output_fd == -1)
                errExit("output filename open");
            // write to stdout and file
            copy(input_fd, output_fd);
            close(input_fd);
            close(output_fd);
        }
    } else {
        cmdLineErr("Must contain input and output filenames: %s\n", argv[0]);
    }
    exit(EXIT_SUCCESS);
}
