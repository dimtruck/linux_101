#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include "lib/tlpi_hdr.h"

void read_write_stdin(int fd) {
    // each block will be 64 bytes
    size_t len = 64;
    char *buf;
    ssize_t numRead, numWritten;

    while(TRUE) {
        // next, read from stdin
        // allocate 64 bytes to buffer
        buf = malloc(len);
        if (buf == NULL)
            errExit("malloc");
        // read from standard in up to 64 bytes
        numRead = read(STDIN_FILENO, buf, len);
        // check if failed to read or end of file
        if (numRead == -1)
            errExit("read");
        if (numRead == 0) {
            // break out of the loop when reached end of file
            break;
        } else {
            // write up to 64 bytes to file
            numWritten = write(fd, buf, numRead);
            if (numWritten == -1)
                errExit("write");
            // write up to 64 bytes to standard in
            numWritten = write(STDOUT_FILENO, buf, numRead);
            if (numWritten == -1)
                errExit("write");
        }

        free(buf);
    }
}

/*
tee command
parameters are:
-a - append file if flag is present
<filename> - file to create or append to
*/
int main(int argc, char *argv[])
{
    int fd;
    // make sure enough arguments are provided
    if (argc > 1 ) {
        // check if second arg is help and output manual
        if (strcmp(argv[1], "--help") == 0) {
            usageErr("%s {-a} filename\n",
                    argv[0]);
        } else if (strcmp(argv[1], "-a") == 0) {
            // check for > 3 parameters
            if (argc > 3) {
                cmdLineErr("Cannot contain more than one filename\n");
            } else {
                // append
                // first, open the file for read-write with READ-WRITE and APPEND flags
                fd = open(argv[2], O_RDWR | O_APPEND,
                            S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                            S_IROTH | S_IWOTH);                     /* rw-rw-rw- */
                if (fd == -1)
                    errExit("open");
                // write to stdout and file
                read_write_stdin(fd);
                close(fd);
            }
        } else {
            // open file with read write + create + truncate
            fd = open(argv[1], O_RDWR | O_CREAT | O_TRUNC,
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                        S_IROTH | S_IWOTH);                     /* rw-rw-rw- */
            if (fd == -1)
                errExit("open");
            // write to stdout and file
            read_write_stdin(fd);
            close(fd);
        }
    } else {
        cmdLineErr("Must contain a filename: %s\n", argv[0]);
    }
    exit(EXIT_SUCCESS);
}
