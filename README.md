# Kernel 101

## Images

### Ubuntu 17.04

Get all kernel headers

```
docker build -t ubuntu-kernel-test -f dockers/Dockerfile.ubuntu .
docker run -ti --rm -v $(pwd):/opt -w=/opt ubuntu-kernel-test bash
```

Run program:

```
gcc -v

pushd lib && gcc -isystem -pthread -c *.c && popd # to get all dependencies

gcc <program>.c lib/*.o
./a.out <params>
```

#### Read write seek

```
gcc -v

pushd lib && gcc -isystem -pthread -c *.c && popd # to get all dependencies

# gcc read_write_seek.c lib/*.o
# touch tfile
./a.out tfile s100000 wabc # seek to offset 100,000 bytes, write abc
# ls -l tfile 
-rw-r--r-- 1 root root 100003 Aug 12 03:07 tfile
# ./a.out tfile s10000 R5
s10000: seek succeeded
R5: 00 00 00 00 00 
```

#### tee

```
# gcc tee.c lib/*.o -o tee
# ./tee output.file
we are going to create
we
we
will
will
create
create
^C
# ./tee -a output.file
and
and
will
will
append
append
^C
# cat output.file
we
will
create
and
will
append
```